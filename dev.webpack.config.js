var path = require("path");
var webpack = require('webpack');
var nib = require('nib');
var noProcessModulesRegExp = /node_modules\/(angular|prismjs)/;

module.exports = {
    entry: {
        app: [
            "./app/index.js",
            'webpack/hot/dev-server',
            'webpack-dev-server/client?http://localhost:8080/',
        ]
    },
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/build/",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                cacheDirectory: true,
                plugins: [
                    'transform-decorators-legacy',
                ],
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            },

            {
                test: /\.css$/,
                // ExtractTextPlugin breaks HMR for CSS
                loader: 'style!css!autoprefixer?browsers=last 2 version'
                //loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true'
            },
            {
                test: /\.styl$/,
                // ExtractTextPlugin breaks HMR for CSS
                loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true&resolve url=true'
                //loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true'
            },
            {
                test: /\.scss$/,
                // ExtractTextPlugin breaks HMR for CSS
                // loader: 'style!css!autoprefixer?browsers=last 2 version!resolve-url!sass?sourceMap'
                loader: 'style!css!autoprefixer?browsers=last 2 version!resolve-url!sass?sourceMap'
                //loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true'
            },
            {
                test: /\.(png|jpg|gif|woff|eot|otf|ttf|svg)$/,
                loader: 'file?name=/files/[hash].[ext]'
            }
        ],
        noParse: [
            // regexp gets full path with loader like
            // '/js/javascript-nodejs/node_modules/client/angular.js'
            // or even
            // '/js/javascript-nodejs/node_modules/6to5-loader/index.js?modules=commonInterop!/js/javascript-nodejs/node_modules/client/head/index.js'
            {
                test: function (path) {
                    //console.log(path);
                    return noProcessModulesRegExp.test(path);
                }
            }
        ]
    },

    stylus: {
        use: [nib()],
        import: ['~nib/lib/nib/index.styl']
    },

    devServer: {
        host: '0.0.0.0',
        hot: true
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
};